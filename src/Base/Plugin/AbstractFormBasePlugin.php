<?php

namespace Drupal\easy_list_builder\Base\Plugin;

use Drupal\easy_list_builder\Interfaces\EasyListBuilderPluginInterface;
use Drupal\easy_list_builder\Plugin\Field\FieldType\EasyListBuilderFieldType;
use Drupal\easy_list_builder\Traits\EasyListBuilderPluginTraits;

/**
 * Class AbstractFormBasePlugin
 *
 * Permet de gérer des plugins qui renvoie des listes issues de FormBase
 *
 * @package Drupal\easy_list_builder\Base\Plugin
 */
abstract class AbstractFormBasePlugin implements EasyListBuilderPluginInterface {
  use EasyListBuilderPluginTraits;

  /**
   * Easy List Builder.
   *
   * @var \Drupal\easy_list_builder\Base\EasyListBuilder\AbstractEasyListBuilderForm
   */
  protected $easyListBuilder;

  /**
   * Retourne la liste.
   *
   * @return string
   */
  abstract public function getEasyListBuilderFormClass();


  /**
   * {@inheritdoc}
   */
  public function getEasyListBuilder(array $configuration, $nbItemsPerPage, EasyListBuilderFieldType $field) {
    if (is_null($this->EasyListBuilder)) {
      $this->EasyListBuilder = $this->createEasyListBuilder($configuration, $nbItemsPerPage, $field);
    }

    return $this->EasyListBuilder;
  }

  /**
   * {@inheritdoc}
   */
  public function createEasyListBuilder(array $configuration, $nbItemsPerPage, EasyListBuilderFieldType $field = null) {
    $easy_list_builder = \Drupal::classResolver($this->getEasyListBuilderFormClass());
    $easy_list_builder->setPlugin($this);
    $easy_list_builder->setConfiguration($configuration);
    $easy_list_builder->setNbItemsPerPage($nbItemsPerPage);
    $easy_list_builder->setField($field);
    return $easy_list_builder;
  }

}
