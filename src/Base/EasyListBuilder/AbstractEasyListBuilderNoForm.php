<?php
/**
 * Created by PhpStorm.
 * User: tsecher
 * Date: 01/04/2019
 * Time: 18:00
 */

namespace Drupal\easy_list_builder\Base\EasyListBuilder;

use Drupal\easy_list_builder\Interfaces\EasyListBuilderInterface;
use Drupal\easy_list_builder\Parameters\EasyListBuilderParameters;
use Drupal\easy_list_builder\Parameters\EasyListBuilderParametersManipulator;
use Drupal\easy_list_builder\Traits\EasyListBuilderTraits;

abstract class AbstractEasyListBuilderNoForm implements EasyListBuilderInterface{

  use EasyListBuilderTraits;

  /**
   * Ids des résultats.
   *
   * @var int[]
   */
  protected $contentIds;

  /**
   * Retourne la query de base sans range.
   *
   * @return mixed
   */
  abstract protected function getBaseQuery(EasyListBuilderParameters $parameters);

  /**
   * Retourne la liste des ids de nodes à afficher.
   *
   * @return int[]
   */
  abstract public function getPageEntityIds(EasyListBuilderParameters $parameters);

  /**
   * {@inheritdoc}
   */
  public function getForm(EasyListBuilderParameters $parameters) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getPager(EasyListBuilderParameters $parameters) {
    \Drupal::request()->query->set('page', $parameters->getCurrentPage());
    pager_default_initialize($this->getTotalCount($parameters), $this->getNbItemsPerPage());
    return ['#type' => 'pager'];
  }

  /**
   * {@inheritdoc}
   */
  public function getTotalCount(EasyListBuilderParameters $parameters) {
    $query = $this->getBaseQuery($parameters);
    return $query->count()->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function getPageItemsCount(EasyListBuilderParameters $parameters) {
    return count($this->getContentIds($parameters));
  }

  /**
   * {@inheritdoc}
   */
  public function getParametersManipulator() {
    return new EasyListBuilderParametersManipulator();
  }

  /**
   * Retourne la liste de résultats.
   */
  public function getContentIds(EasyListBuilderParameters $parameters) {
    if( is_null($this->contentIds) ){
      $this->contentIds = $this->getPageEntityIds($parameters);
    }
    return $this->contentIds;
  }


}
