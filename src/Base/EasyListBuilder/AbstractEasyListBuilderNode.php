<?php
/**
 * Created by PhpStorm.
 * User: tsecher
 * Date: 28/03/2019
 * Time: 17:28
 */

namespace Drupal\easy_list_builder\Base\EasyListBuilder;


use Drupal\easy_list_builder\Parameters\EasyListBuilderParameters;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;

abstract class AbstractEasyListBuilderNode extends AbstractEasyListBuilderEntity {

  /**
   * {@inheritdoc}
   */
  protected function getEntityTypeId() {
    return 'node';
  }

}
