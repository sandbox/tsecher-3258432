<?php

namespace Drupal\easy_list_builder\Base\EasyListBuilder;

use Drupal\easy_list_builder\Interfaces\EasyListBuilderInterface;
use Drupal\easy_list_builder\Parameters\EasyListBuilderParameters;
use Drupal\easy_list_builder\Service\EasyListBuilder;
use Drupal\easy_list_builder\Traits\EasyListBuilderTraits;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormState;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class AbstractEasyListBuilderForm.
 *
 * Gestion des easy list builderes liées à un formulaire drupal ajax.
 *
 * @package Drupal\easy_list_builder\Base\Form
 */
abstract class AbstractEasyListBuilderForm extends FormBase implements EasyListBuilderInterface {

  use EasyListBuilderTraits;

  /**
   * Liste des commandes.
   *
   * @var \Drupal\Core\Ajax\CommandInterface
   */
  protected $commands;

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // @mute
  }

  /**
   * {@inheritdoc}
   */
  public function getForm(EasyListBuilderParameters $parameters) {
    $formState = new FormState();
    $formState->setFormObject($this);
    $formState->addBuildInfo('parameters', $parameters);

    /** @var \Drupal\easy_list_builder\Decorador\FormBuilderDecorador $formBuilder */
    $formBuilder = \Drupal::formBuilder();
    return $formBuilder->EasyListBuilderBuildForm(static::class, $formState);
  }

  /**
   * Action ajax.
   */
  public function onAjaxSubmit(array &$form, FormStateInterface $formState) {
    $parameters = EasyListBuilderParameters::create($this, $formState);


    if ($this->mustReplaceForm($form, $formState, $parameters)) {
      $this->addCommand(
        new ReplaceCommand(
          '[' . EasyListBuilder::ATTRIBUTE_FORM . '="' . $this->getId() . '"]',
          EasyListBuilder::me()->buildForm($this)
        )
      );
    }

    if ($this->mustReplaceList($form, $formState, $parameters)) {
      $this->addCommand(
        new ReplaceCommand(
          '[' . EasyListBuilder::ATTRIBUTE_LIST . '="' . $this->getId() . '"]',
          EasyListBuilder::me()->buildList($this)
        )
      );
    }

    if ($this->mustReplacePager($form, $formState, $parameters)) {
      $this->addCommand(
        new ReplaceCommand(
          '[' . EasyListBuilder::ATTRIBUTE_PAGER . '="' . $this->getId() . '"]',
          EasyListBuilder::me()->buildPager($this)
        )
      );
    }

    // Ajout des commandes de fin sur la gestion des urls.
    $url = EasyListBuilder::me()->buildUrlQuery($this, $parameters);
    $this->addCommand(
      new InvokeCommand(
        '[' . EasyListBuilder::ATTRIBUTE_LIST . '="' . $this->getId() . '"]',
        'easy_list_builderUpdateUrl',
        [$url]
      )
    );

    return $this->getResponse();
  }

  /**
   * Ajoute une commande au tabelau de commande.
   */
  protected function addCommand($command) {
    $this->commands[] = $command;
  }

  /**
   * Retourne l'ajax response.
   *
   * @return AjaxResponse
   */
  protected function getResponse() {
    $response = new AjaxResponse();
    foreach ($this->commands as $command) {
      $response->addCommand($command);
    }
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function mustReplaceForm(array &$form, FormStateInterface $formState, EasyListBuilderParameters $parameters) {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function mustReplaceList(array &$form, FormStateInterface $formState, EasyListBuilderParameters $parameters) {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function mustReplacePager(array &$form, FormStateInterface $formState, EasyListBuilderParameters $parameters) {
    return TRUE;
  }

  /**
   * Retourne la liste des paramètres d'url.
   *
   * @return array|null
   */
  public function getUrlQueryFromParameters(EasyListBuilderParameters $parameters) {
    return NULL;
  }

  /**
   * Retourne la liste des paramètres depuis les données d'url.
   *
   * @param array $queryParameters
   *
   * @return array|null
   */
  public function getParametersFromQuery(array $queryParameters) {
    return NULL;
  }

  /**
   * Retourne les paramètres courant en fonction du formstate.
   *
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *
   * @return EasyListBuilderParameters
   */
  public function getParametersFromFormState(FormStateInterface $formState){
    return $formState->getBuildInfo()['parameters'];
  }

}
