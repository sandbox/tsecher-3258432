<?php
/**
 * Created by PhpStorm.
 * User: tsecher
 * Date: 28/03/2019
 * Time: 17:28
 */

namespace Drupal\easy_list_builder\Base\EasyListBuilder;


use Drupal\easy_list_builder\Parameters\EasyListBuilderParameters;
use Drupal\easy_list_builder\Traits\EasyListBuilderEntityTrait;

abstract class AbstractEasyListBuilderEntity extends AbstractEasyListBuilderForm {

  use EasyListBuilderEntityTrait;

  /**
   * Liste des ids de contenus de la page
   *
   * @var array
   */
  protected $entityIds;

  /**
   * Retourne l'id de l'entité.
   *
   * @return mixed
   */
  abstract protected function getEntityTypeId();

  /**
   * Retourne l'id du view mode utilisé.
   *
   * @return string
   */
  abstract protected function getViewMode();

  /**
   * Retourne la query de base sans range.
   *
   * @return mixed
   */
  abstract protected function getBaseQuery(EasyListBuilderParameters $parameters);

  /**
   * Retourne la liste des ids de entités à afficher.
   *
   * @return int[]
   */
  abstract public function getPageEntityIds(EasyListBuilderParameters $parameters);

  /**
   * {@inheritdoc}
   */
  public function getPager(EasyListBuilderParameters $parameters) {
    \Drupal::request()->query->set('page', $parameters->getCurrentPage());

    \Drupal::service('pager.manager')
      ->createPager($this->getTotalCount($parameters), $this->getNbItemsPerPage());

    return ['#type' => 'pager'];
  }

  /**
   * {@inheritdoc}
   */
  public function getTotalCount(EasyListBuilderParameters $parameters) {
    $query = $this->getBaseQuery($parameters);
    return $query->count()->execute();
  }

  /**
   * Retourne la liste de résultats.
   */
  public function getContentIds(EasyListBuilderParameters $parameters) {
    if (is_null($this->entityIds)) {
      $this->entityIds = $this->getPageEntityIds($parameters);
    }

    return $this->entityIds;
  }

}
