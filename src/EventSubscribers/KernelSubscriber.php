<?php
/**
 * Created by PhpStorm.
 * User: tsecher
 * Date: 01/04/2019
 * Time: 11:51
 */

namespace Drupal\easy_list_builder\EventSubscribers;


use Drupal\easy_list_builder\Parameters\EasyListBuilderParameters;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class KernelSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      KernelEvents::REQUEST => 'onKernelRequest'
    ];
  }

  /**
   * Redirige vers la page sans page=0 pour éviter le duplicate.
   *
   * @param \Symfony\Component\HttpKernel\Event\GetResponseEvent $event
   */
  public function onKernelRequest(GetResponseEvent $event) {
    if( !\Drupal::request()->isXmlHttpRequest() ){
      $query = \Drupal::request()->query;
      if( $query->has(EasyListBuilderParameters::KEY_PAGE)){
        if( $query->get(EasyListBuilderParameters::KEY_PAGE) == 0 ){
          $params = $query->all();
          unset($params[EasyListBuilderParameters::KEY_PAGE]);
          if (!empty($params)) {
            // On réordonne les paramètres.
            ksort($params);
            $query = '?' . http_build_query($params);
          }
          else {
            $query = '';
          }

          // On redirige.
          $redirectResponse = new RedirectResponse(\Drupal::request()
              ->getPathInfo() . $query, 308);
          $redirectResponse->send();
          exit;
        }
      }
    }
  }


}
