<?php
/**
 * Created by PhpStorm.
 * User: tsecher
 * Date: 27/03/2019
 * Time: 12:22
 */

namespace Drupal\easy_list_builder\Decorador;

use Drupal\Core\Form\FormBuilder;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class FormBuilderDecorador
 *
 * Décore le form builder pour ne pas réinitialiser le form Object d'un formstate
 * dans la fonction buildForm.
 *
 * @package Drupal\easy_list_builder\Decorador
 */
class FormBuilderDecorador extends FormBuilder {

  /**
   * Empèche la sucharge de l'objet form à la construcion du formulaire.
   *
   * @param $formId
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *
   * @throws \Drupal\Core\Form\EnforcedResponseException
   * @throws \Drupal\Core\Form\FormAjaxException
   */
  public function EasyListBuilderBuildForm($formId, FormStateInterface $formState) {
    return $this->buildForm($formId, $formState);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId($form_arg, FormStateInterface &$form_state) {
    $back_trace = debug_backtrace();

    /**
     * Si on est dans le cas d'un point d'entrée custom, on ne modifie pas forcément
     * L'objet en entrée.
     */
    if( $back_trace[2]['function'] === 'EasyListBuilderBuildForm' ){
      return $this->EasyListBuilderGetFormId($form_arg, $form_state);
    }

    return parent::getFormId($form_arg, $form_state);
  }

  /**
   * Surchage de la gonction getFormId.
   *
   * @param $form_arg
   * @param $form_state
   */
  protected function EasyListBuilderGetFormId($form_arg, FormStateInterface $form_state) {
    if( $form_state->getFormObject() ){
      return $form_state->getFormObject()->getFormId();
    }
    else{
      return $this->getFormId($form_arg, $form_state);
    }
  }

}
