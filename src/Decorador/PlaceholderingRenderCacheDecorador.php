<?php
/**
 * Created by PhpStorm.
 * User: tsecher
 * Date: 27/03/2019
 * Time: 17:33
 */

namespace Drupal\easy_list_builder\Decorador;


use Drupal\Core\Render\PlaceholderingRenderCache;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class PlaceholderingRenderCacheDecorador.
 *
 * Redéfinie la fonction get pour aller chercher dans le cache en méthode POST.
 *
 * @package Drupal\easy_list_builder\Decorador
 */
class PlaceholderingRenderCacheDecorador extends PlaceholderingRenderCache {

  /**
   * {@inheritdoc}
   */
  public function get(array $elements) {
    if( $this->isRedefineContext() ){
      return $this->redefineGet($elements);
    }
    else{
      return parent::get($elements);
    }
  }

  /**
   * Redéfinition de la méthode get.
   *
   * @param array $elements
   *
   * @return array|bool|false
   */
  protected function redefineGet(array $elements) {
    // When rendering placeholders, special case auto-placeholdered elements:
    // avoid retrieving them from cache again, or rendering them again.
    if (isset($elements['#create_placeholder']) && $elements['#create_placeholder'] === FALSE) {
      $cached_placeholder_result = $this->getFromPlaceholderResultsCache($elements);
      if ($cached_placeholder_result !== FALSE) {
        return $cached_placeholder_result;
      }
    }

    $cached_element = parent::get($elements);

    if ($cached_element === FALSE) {
      return FALSE;
    }
    else {
      if ($this->placeholderGenerator->canCreatePlaceholder($elements) && $this->placeholderGenerator->shouldAutomaticallyPlaceholder($cached_element)) {
        return $this->createPlaceholderAndRemember($cached_element, $elements);
      }

      return $cached_element;
    }
  }

  /**
   * Identifie le contexte de surcharge.
   *
   * @return bool
   */
  protected function isRedefineContext() {
    $request = $this->requestStack->getCurrentRequest();
    return $request->getMethod() === Request::METHOD_POST;
  }

}
