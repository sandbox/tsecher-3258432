<?php

namespace Drupal\easy_list_builder\Traits;

use Drupal\easy_list_builder\Plugin\Field\FieldFormatter\EasyListBuilderFieldFormatter;
use Drupal\Core\Entity\Entity\EntityViewMode;

/**
 * Trait EasyListBuilderPluginEntityTrait.
 *
 * Ajoute :
 *  - la notion de view mode dans les éléments à rendre.
 *
 * @package Drupal\easy_list_builder\Traits
 */
trait EasyListBuilderPluginEntityTrait {

  /**
   * {@inheritdoc}
   */
  public function getViewModeForm($viewModeField, $targetEntityType, $bundle, array $configuration, array $stateCondition) {
    // Récupération de la liste des view modes dispo.
    $viewModesOptions = $this->getViewModesOptions($targetEntityType, $bundle);
    return [
      '#type'          => 'select',
      '#options'       => $viewModesOptions,
      '#title'         => t('View mode'),
      '#default_value' => array_key_exists($viewModeField, $configuration) ? $configuration[$viewModeField] : NULL,
      '#states'        => ['required' => $stateCondition],
    ];
  }

  /**
   * retourne les options de view_modes.
   */
  protected function getViewModesOptions($targetEntityType, $bundle) {

    // INitialisation de la condtion de recherche.
    $properties = ['targetEntityType' => $targetEntityType];
    if(!empty($bundle)){
      $properties['bundle']= $bundle;
    }

    // Récuépration des view modes.
    /** @var \Drupal\Core\Entity\Entity\EntityViewDisplay[] $viewDisplays */
    $viewDisplays = \Drupal::entityTypeManager()
      ->getStorage('entity_view_display')
      ->loadByProperties($properties);
    $options = ['full' => t('Full')];
    foreach ($viewDisplays as $viewDisplay) {
      $viewModeId = $viewDisplay->getMode();
      if ($viewMode = EntityViewMode::load($targetEntityType . '.' . $viewModeId)) {
        $options[$viewModeId] = $viewMode->label();
      }
    }
    return $options;
  }

  /**
   * Retourne le formulaire de sélection des bundle d'un type d'entité
   */
  protected function getBundleForm($bundleField, $targetEntityType, array $configuration, array $stateCondition) {
    // Récupération de la liste des view modes dispo.
    $viewModesOptions = $this->getBundlesOptions($targetEntityType);
    return [
      '#type'          => 'checkboxes',
      '#options'       => $viewModesOptions,
      '#title'         => t('Type'),
      '#default_value' => array_key_exists($bundleField, $configuration) ? $configuration[$bundleField] : [],
      '#states'        => ['required' => $stateCondition],
      '#multiple'      => TRUE,
    ];
  }

  /**
   * Retourne les options de view_modes.
   */
  protected function getBundlesOptions($targetEntityType) {
    $options = \Drupal::service('entity_type.bundle.info')
      ->getBundleInfo($targetEntityType);
    $options = array_combine(array_keys($options), array_column($options, 'label'));
    return $options;
  }

  /**
   * Retourne le view mode courant du field_formatter.
   */
  protected function getViewMode() {
    if( $fieldFormatter = $this->getFieldFormatter() ){
      return $fieldFormatter->getSetting(EasyListBuilderFieldFormatter::FIELD_VIEW_MODE);
    }
    return 'teaser';
  }
}
