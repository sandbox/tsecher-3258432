<?php


namespace Drupal\easy_list_builder\Traits;


use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\easy_list_builder\Parameters\EasyListBuilderParameters;

trait EasyListBuilderEntityTrait {

  /**
   * Liste des ids de contenus de la page
   *
   * @var array
   */
  protected $entityIds;

  /**
   * Retourne l'id de l'entité.
   *
   * @return mixed
   */
  abstract protected function getEntityTypeId();

  /**
   * Retourne l'id du view mode utilisé.
   *
   * @return string
   */
  abstract protected function getViewMode();

  /**
   * Retourne la query de base sans range.
   *
   * @return mixed
   */
  abstract protected function getBaseQuery(EasyListBuilderParameters $parameters);

  /**
   * Retourne le plugin associé.
   *
   * @return \Drupal\easy_list_builder\Interfaces\EasyListBuilderPluginInterface
   */
  abstract public function getPlugin();

  /**
   * Retourne la liste des ids de entités à afficher.
   *
   * @return int[]
   */
  abstract public function getPageEntityIds(EasyListBuilderParameters $parameters);

  /**
   * {@inheritdoc}
   */
  public function getList(EasyListBuilderParameters $parameters) {
    $contentIds = $this->getContentIds($parameters);
    if (!empty($contentIds)) {
      $entities = \Drupal::entityTypeManager()
        ->getStorage($this->getEntityTypeId())
        ->loadMultiple($contentIds);
      return \Drupal::entityTypeManager()
        ->getViewBuilder($this->getEntityTypeId())
        ->viewMultiple($entities, $this->getViewMode());
    }
    return [];
  }

  /**
   * @param \Drupal\easy_list_builder\Parameters\EasyListBuilderParameters $parameters
   *
   * @return int
   */
  public function getPageItemsCount(EasyListBuilderParameters $parameters) {
    return count($this->getContentIds($parameters));
  }

}
