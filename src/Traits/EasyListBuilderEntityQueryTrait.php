<?php

namespace Drupal\easy_list_builder\Traits;

use Drupal\Core\Entity\Query\QueryInterface;

/**
 * Trait EasyListBuilderEntityQueryTrait.
 *
 * Override des fonctions basiques pour matcher l'interface des entity uery.
 * @package Drupal\easy_list_builder\Traits
 */
trait EasyListBuilderEntityQueryTrait {

  /**
   * Retourne les ids des entités selon la query.
   *
   * @param \Drupal\Core\Entity\Query\QueryInterface $query
   *   The query.
   *
   * @return array|int
   *   The list of ids.
   */
  protected function getEntityIdsFromQuery(QueryInterface $query) {
    return $query->execute();
  }
}
