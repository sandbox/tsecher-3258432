<?php
/**
 * Created by PhpStorm.
 * User: tsecher
 * Date: 02/04/2019
 * Time: 09:49
 */

namespace Drupal\easy_list_builder\Traits;


use Drupal\easy_list_builder\Parameters\EasyListBuilderParameters;

trait EasyListBuilderCustomPaginationTrait {


  /**
   * {@inheritdoc}
   *
   * @return int
   */
  abstract public function getTotalCount(EasyListBuilderParameters $parameters);

  /**
   * {@inheritdoc}
   *
   * @return int
   */
  abstract public function getNbItemsPerPage();

  /**
   * {@inheritdoc}
   *
   * return int
   */
  abstract public function getFirstPageDelta(EasyListBuilderParameters $parameters);

  /**
   * {@inheritdoc}
   */
  public function getPager(EasyListBuilderParameters $parameters) {
    \Drupal::request()->query->set('page', $parameters->getCurrentPage());
    \Drupal::service('pager.manager')->createPager($this->getTotalCount($parameters) + $this->getFirstPageDelta($parameters), $this->getNbItemsPerPage());

    return [
      '#type' => 'pager'
    ];
  }

  /**
   * Retourne l'index de la page courante.
   * @return int
   */
  public function getCurrentPageIndex() {
    return \Drupal::service('pager.parameters')->findPage();
  }

  /**
   * Retourne le nombre d'éléments par page à rechercher. On exclut les higlhits.
   *
   * @return int
   */
  public function getNbItemsForCurrentPage(EasyListBuilderParameters $parameters) {
    if ($this->getFirstPageDelta($parameters) > 0) {
      // Si on est en première page alor le nombre de résultats est nbPage - delta
      $currentPageIndex = $this->getCurrentPageIndex();
      if ($currentPageIndex === 0) {
        return $this->getNbItemsPerPage() - $this->getFirstPageDelta($parameters);
      }
    }

    return $this->getNbItemsPerPage();
  }

  /**
   * Retourne l'index du premier élément de la page courante.
   *
   * @return int
   */
  protected function getCurrentPageFirstItemIndex(EasyListBuilderParameters $parameters) {
    // Récupération de l'index de la page courante.
    $currentPageIndex = $this->getCurrentPageIndex();

    // Si on est sur la premièrepage, on retourne 0, on ne peut pas être négatif.
    if ($currentPageIndex == 0) {
      return $currentPageIndex;
    }

    return $this->getNbItemsForCurrentPage($parameters) * $currentPageIndex - $this->getFirstPageDelta($parameters);
  }

}
