<?php


namespace Drupal\easy_list_builder\Traits;


use Drupal\easy_list_builder\Parameters\EasyListBuilderParameters;
use Drupal\search_api\Query\Query;

/**
 * Trait EasyListBuilderSearchApiQueryTrait.
 *
 * Override des fonctions basiques pour matcher l'interface de query de SearchApi.
 * @package Drupal\easy_list_builder\Traits
 */
trait EasyListBuilderSearchApiQueryTrait {


  /**
   * {@inheritdoc}
   */
  public function getTotalCount(EasyListBuilderParameters $parameters) {
    /** @var \Drupal\search_api\Query\QueryInterface $query */
    $query = $this->getBaseQuery($parameters);

    return $query->execute()->getResultCount();
  }

  /**
   * Retourne les ids des entités selon la query.
   */
  protected function getEntityIdsFromQuery(Query $query) {

    $ids = [];
    /** @var \Drupal\search_api\Item\Item $resultItem */
    foreach ($query->execute()->getResultItems() as $key => $resultItem){

      try{
        if( $object = $resultItem->getOriginalObject() ){
          if( $entity = $object->getEntity() ){
            $ids[] = $entity->id();
          }
        }
      }
      catch(\Exception $e){
          // Mute exception...
      }
    }

    return $ids;
  }
}
