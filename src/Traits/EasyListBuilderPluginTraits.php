<?php
/**
 * Created by PhpStorm.
 * User: tsecher
 * Date: 26/03/2019
 * Time: 16:31
 */

namespace Drupal\easy_list_builder\Traits;


trait EasyListBuilderPluginTraits {

  /**
   * La définition du plugin.
   *
   * @var array
   */
  protected $pluginDefinitions;

  /**
   * Modifie les plugin definitions
   *
   * @return void
   */
  public function setPluginDefinitions(array $pluginDefinitions){
    $this->pluginDefinitions = $pluginDefinitions;
  }

  /**
   * Retourne les plugin definitions.
   *
   * @return array
   */
  public function getPluginDefinitions(){
    return $this->pluginDefinitions;
  }

  /**
   * {@inheritdoc}
   */
  public function label(){
    return $this->pluginDefinitions['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function description(){
    return $this->pluginDefinitions['description'];
  }

  /**
   * {@inheritdoc}
   */
  public function pluginId(){
    return $this->pluginDefinitions['id'];
  }

}
