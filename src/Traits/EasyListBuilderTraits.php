<?php

namespace Drupal\easy_list_builder\Traits;

use Drupal\easy_list_builder\Interfaces\EasyListBuilderPluginInterface;
use Drupal\easy_list_builder\Plugin\Field\FieldFormatter\EasyListBuilderFieldFormatter;
use \Drupal\easy_list_builder\Parameters\EasyListBuilderParameters;
use Drupal\easy_list_builder\Plugin\Field\FieldType\EasyListBuilderFieldType;

trait EasyListBuilderTraits {

  /**
   * Plugin.
   *
   * @var \Drupal\easy_list_builder\Interfaces\EasyListBuilderPluginInterface
   */
  protected $plugin;

  /**
   * Configuration.
   *
   * @var array
   */
  protected $configuration;

  /**
   * Nombre d'items par page.
   *
   * @var int
   */
  protected $nbItemsPerPage;

  /**
   * Champ associé à la liste.
   *
   * @var \Drupal\easy_list_builder\Plugin\Field\FieldType\EasyListBuilderFieldType
   */
  protected $field;

  /**
   * Formatter associé.
   *
   * @var EasyListBuilderFieldFormatter
   */
  protected $fieldFormatter;

  /**
   * {@inheritdoc}
   */
  public function setPlugin(EasyListBuilderPluginInterface $plugin) {
    $this->plugin = $plugin;
  }

  /**
   * {@inheritdoc}
   */
  public function getPlugin() {
    return $this->plugin;
  }

  /**
   * @return array
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * @param array $configuration
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = $configuration;
  }

  /**
   * @return int
   */
  public function getNbItemsPerPage() {
    return $this->nbItemsPerPage;
  }

  /**
   * @param int $nbItemsPerPage
   */
  public function setNbItemsPerPage($nbItemsPerPage) {
    $this->nbItemsPerPage = $nbItemsPerPage;
  }

  /**
   * @return \Drupal\easy_list_builder\Plugin\Field\FieldType\EasyListBuilderFieldType
   */
  public function getField() {
    return $this->field;
  }

  /**
   * @param \Drupal\easy_list_builder\Plugin\Field\FieldType\EasyListBuilderFieldType $field
   */
  public function setField(EasyListBuilderFieldType $field = NULL) {
    $this->field = $field;
  }

  /**
   * {@inheritdoc}
   */
  public function setFieldFormatter(EasyListBuilderFieldFormatter $formatter) {
    $this->fieldFormatter = $formatter;
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldFormatter() {
    return $this->fieldFormatter;
  }

  /**
   * Retourne les infos autres à envoyer aux tempalte de liste.
   *
   * @param \Drupal\easy_list_builder\Parameters\EasyListBuilderParameters $parameters
   *
   * @return mixed
   */
  public function getOtherInfo(EasyListBuilderParameters $parameters = NULL) {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getId() {
    return $this->getPlugin()->pluginId();
  }

}
