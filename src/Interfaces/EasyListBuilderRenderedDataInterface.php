<?php

namespace Drupal\easy_list_builder\Interfaces;


use Drupal\easy_list_builder\Parameters\EasyListBuilderParameters;

interface EasyListBuilderRenderedDataInterface {

  public function alterBuildArray(array &$buildArray, EasyListBuilderParameters $parameters);

}
