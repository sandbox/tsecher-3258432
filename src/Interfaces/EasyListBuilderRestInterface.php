<?php

namespace Drupal\easy_list_builder\Interfaces;


use Drupal\easy_list_builder\Parameters\EasyListBuilderParameters;

interface EasyListBuilderRestInterface {

  public function getRestFormatList(EasyListBuilderParameters $parameters);

}
