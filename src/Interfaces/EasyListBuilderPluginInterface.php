<?php
/**
 * Created by PhpStorm.
 * User: tsecher
 * Date: 26/03/2019
 * Time: 14:23
 */

namespace Drupal\easy_list_builder\Interfaces;

use Drupal\easy_list_builder\Plugin\Field\FieldType\EasyListBuilderFieldType;
use Drupal\Core\Form\FormStateInterface;

interface EasyListBuilderPluginInterface {

  /**
   * Modifie les plugin definitions
   *
   * @return void
   */
  public function setPluginDefinitions(array $definition);

  /**
   * Retourne les plugin definitions.
   *
   * @return array
   */
  public function getPluginDefinitions();

  /**
   * Retourne le libellé du plugin.
   *
   * @return string
   */
  public function label();

  /**
   * Retoune la description du plugin.
   *
   * @return string
   */
  public function description();

  /**
   * Retourne le plugin ID.
   *
   * @return string
   */
  public function pluginId();

  /**
   * Initialise le formulaire du champs de type liste.
   *
   * @param EasyListBuilderFieldType $item
   * @param FormStateInterface $formState
   * @param array $stateOption
   *   Les options utiles aux states.
   *
   * @return array
   */
  public function getWidgetForm(EasyListBuilderFieldType $field, array $configuration, FormStateInterface $formState, array $stateCondition);

  /**
   * Permet de masser les données du widget avant enregistrement en db.
   *
   * @param array $values
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *
   * @return mixed
   */
  public function getMassagedWidgetValues(array $values, array $form, FormStateInterface $formState);

  /**
   * Retourne l'objet qui gère la liste en tant que tel.
   *
   * @param array $configuration
   * @param $nbItemsPerPage
   * @param EasyListBuilderFieldType $field
   *
   * @return \Drupal\easy_list_builder\Interfaces\EasyListBuilderInterface
   */
  public function getEasyListBuilder(array $configuration, $nbItemsPerPage, EasyListBuilderFieldType $field);

  /**
   * Crée l'objet qui gère la liste en tant que tel sans cache.
   *
   * @param array $configuration
   * @param $nbItemsPerPage
   * @param EasyListBuilderFieldType $field
   *
   * @return \Drupal\easy_list_builder\Interfaces\EasyListBuilderInterface
   */
  public function createEasyListBuilder(array $configuration, $nbItemsPerPage, EasyListBuilderFieldType $field = NULL);

}
