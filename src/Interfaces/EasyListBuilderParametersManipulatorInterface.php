<?php

namespace Drupal\easy_list_builder\Interfaces;


use Drupal\easy_list_builder\Parameters\EasyListBuilderParameters;

interface EasyListBuilderParametersManipulatorInterface {

  /**
   * Ajoute un champ de base
   *
   * @param $name
   * @param $type
   * @param bool $multpile
   *
   * @return mixed
   */
  public function add($name, $type, $multiple = FALSE);

  /**
   * Ajoute un champs custom.
   *
   * @param $name
   * @param null $toUrlCallback
   * @param null $toParametersCallback
   * @param null $initCallback
   * @param bool $multpile
   *
   * @return mixed
   */
  public function addCustom($name, $toUrlCallback = NULL, $toParametersCallback = NULL, $initCallback = NULL, $multiple = FALSE);

  /**
   * Retourne les paramètres d'url depuis les paramètres de liste.
   *
   * @param array $parameters
   *
   * @return array
   */
  public function getUrlQueryFromParameters(array $parameters);

  /**
   * Retourne les paramètres de liste depuis les paramètres d'url.
   *
   * @param array $query
   *
   * @return array
   */
  public function getParametersFromUrlQuery(array $query);

  /**
   * Initialise la liste des paramètres finaux.
   *
   * @param array $parameters
   *
   * @return array
   */
  public function initParameters(array &$parameters);

  /**
   * Retourne vrai si le contenxte à des paramètres actifs.
   *
   * @return bool
   */
  public function hasParams(EasyListBuilderParameters $parameters);

  /**
   * Retourne la liste des paramètres autorisés.
   *
   * @return mixed
   */
  public function getAllowedParameters();

}
