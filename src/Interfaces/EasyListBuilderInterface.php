<?php

namespace Drupal\easy_list_builder\Interfaces;

use Drupal\easy_list_builder\Parameters\EasyListBuilderParameters;
use Drupal\easy_list_builder\Plugin\Field\FieldFormatter\EasyListBuilderFieldFormatter;
use Drupal\easy_list_builder\Plugin\Field\FieldType\EasyListBuilderFieldType;
use Drupal\Core\Entity\EntityInterface;

interface EasyListBuilderInterface {

  /**
   * Modifie le plugin.
   */
  public function setPlugin(EasyListBuilderPluginInterface $plugins);

  /**
   * Retourne le plugin associé.
   *
   * @return \Drupal\easy_list_builder\Interfaces\EasyListBuilderPluginInterface
   */
  public function getPlugin();

  /**
   * Modifie le champ associé.
   */
  public function setField(EasyListBuilderFieldType $field = NULL);

  /**
   * Retourne le champ associé.
   *
   * @return \Drupal\easy_list_builder\Interfaces\EasyListBuilderPluginInterface
   */
  public function getField();

  /**
   * Modifie le foprmatter associé.
   *
   * @param $fieldFormatters
   *
   * @return null
   */
  public function setFieldFormatter(EasyListBuilderFieldFormatter $fieldFormatters);

  /**
   * Retourne le formatter associé
   *
   * @return EasyListBuilderFieldFormatter
   */
  public function getFieldFormatter();

  /**
   * Renvoie la configuraiton de la liste.
   * @return array
   */
  public function getConfiguration();

  /**
   * Modifie la configuration
   **/
  public function setConfiguration(array $configuration);

  /**
   * Retourne le nombre d'élément par page.
   *
   * @return int
   */
  public function getNbItemsPerPage();

  /**
   * Modifie le nombre d'élément par page.
   *
   * @param int $nbItemsPerPage
   */
  public function setNbItemsPerPage($nbItemsPerPage);

  /**
   * Retourn un id de liste pour associer les éléents du dom via les attributs.
   *
   * @return string
   */
  public function getId();

  /**
   * Retourne le build array du formulaire.
   *
   * @return mixed
   */
  public function getForm(EasyListBuilderParameters $parameters);

  /**
   * Retourne le build array de la liste.
   *
   * @return mixed
   */
  public function getList(EasyListBuilderParameters $parameters);

  /**
   * Retourne le build array du pager.
   *
   * @return mixed
   */
  public function getPager(EasyListBuilderParameters $parameters);

  /**
   * Retourne le nombre d'élément total.
   *
   * @return int
   */
  public function getTotalCount(EasyListBuilderParameters $parameters);

  /**
   * @param \Drupal\easy_list_builder\Parameters\EasyListBuilderParameters $parameters
   *
   * @return int
   */
  public function getPageItemsCount(EasyListBuilderParameters $parameters);

  /**
   * Retourne le manipulateur de paramètres.
   *
   * @return \Drupal\easy_list_builder\Interfaces\EasyListBuilderParametersManipulatorInterface
   */
  public function getParametersManipulator();

  /**
   * Retourne vrai si l'entité passé fait partie de la liste courante.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *
   * @return bool
   */
  public function entityIsInDefaultList(EntityInterface $entity);

  /**
   * Retourne les infos autres à envoyer aux tempalte de liste.
   *
   * @param \Drupal\easy_list_builder\Parameters\EasyListBuilderParameters $parameters
   *
   * @return mixed
   */
  public function getOtherInfo(EasyListBuilderParameters $parameters = NULL);

}
