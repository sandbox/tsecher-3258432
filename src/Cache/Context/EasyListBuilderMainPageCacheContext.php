<?php


namespace Drupal\easy_list_builder\Cache\Context;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\Context\CacheContextInterface;

/**
 * Définit un contexte de cache spécifique pour le module politesse.
 *
 * Permet de générer un contexte de cache différent entre 3h et 17h59
 * et entre 18h et 2h59h
 *
 * Cache context ID: 'easy_list_builder_main_page'.
 */
class EasyListBuilderMainPageCacheContext implements CacheContextInterface {

  /**
   * Le contexte
   *
   * @const string
   */
  const CONTEXT_ID = 'easy_list_builder_main_page';

  /**
   * Retourne le service
   *
   * @return static
   *   Le service.
   */
  public static function me() {
    return \Drupal::service('cache_context.easy_list_builder_main_page');
  }

  /**
   * {@inheritdoc}
   */
  public static function getLabel() {
    return t('Cache context to cache only the main page of a easy list builder');
  }

  /**
   * Renvoie l'identifiant de contexte courant.
   *
   * On renvoie un identifiant de contexte différent
   * 1 si on est entre 3h et 17h59
   * 0 si on est entre 18h et 2h59h
   *
   * {@inheritdoc}
   */
  public function getContext() {
    // Si des paramètres d'urls sont passé, on renvoi params.
    return $this->isMainPageContext() ? 1 : 0;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata() {
    return new CacheableMetadata();
  }

  /**
   * Si l'url n'a pas de paramètres d'url on estime qu'on est en page de
   * contenu.
   *
   * @return boll
   *   L'état de la journée.
   */
  public function isMainPageContext() {
    $allParameters = array_filter(\Drupal::request()->query->all(), function ($item, $key) {
      return !($item === 0 && $key === 'page');
    }, ARRAY_FILTER_USE_BOTH);

    return empty($allParameters);
  }

}
