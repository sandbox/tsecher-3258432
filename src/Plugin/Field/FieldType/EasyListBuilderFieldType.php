<?php

namespace Drupal\easy_list_builder\Plugin\Field\FieldType;

use Drupal\easy_list_builder\Service\EasyListBuilderPluginManager;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'easy_list_builder_field_type' field type.
 *
 * @FieldType(
 *   id = "easy_list_builder_field_type",
 *   label = @Translation("Easy List Builder field type"),
 *   description = @Translation("Gestionnaire de liste"),
 *   default_widget = "easy_list_builder_widget",
 *   default_formatter = "easy_list_builder_field_formatter"
 * )
 *
 */
class EasyListBuilderFieldType extends FieldItemBase {

  /**
   * Type de plugin
   *
   * @const string
   */
  const FIELD_PLUGIN_ID = 'plugin_id';

  /**
   * Plugin définitions
   *
   * @const string
   */
  const FIELD_CONFIGURATION = 'plugin_defintion';

  /**
   * Easy List Builder
   *
   * @var \Drupal\easy_list_builder\Interfaces\EasyListBuilderInterface
   */
  protected $easy_list_builder = FALSE;

  /**
   * @var array
   */
  public static $fields = [
    self::FIELD_PLUGIN_ID         => [
      'label'      => 'Type de liste',
      'type'       => 'string',
      'schema'     => [
        'type'   => 'varchar',
        'length' => 100,
      ],
      'input_data' => [
        '#type' => 'select',
      ],
    ],
    self::FIELD_CONFIGURATION     => [
      'label'      => 'Data',
      'type'       => 'string',
      'schema'     => [
        'type' => 'text',
      ],
      'input_data' => [
        '#type' => 'textfield',
      ],
    ],
  ];


  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {

    $properties = [];

    foreach (static::$fields as $fieldName => $fieldData) {
      // File reference
      $properties[$fieldName] = DataDefinition::create($fieldData['type'])
        ->setLabel(t($fieldData['label']));
    }

    return $properties;
  }


  /**
   * Init table schema
   *
   * @param \Drupal\Core\Field\FieldStorageDefinitionInterface $field_definition
   *
   * @return array
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {

    $columns = [];
    foreach (static::$fields as $fieldName => $fieldData) {
      $columns[$fieldName] = $fieldData['schema'];
    }

    return [
      'columns' => $columns
    ];
  }

  /**
   * @return bool
   */
  public function isEmpty() {
    $pluginId = $this->get(static::FIELD_PLUGIN_ID)->getValue();
    return empty($pluginId);
  }

  /**
   * Serialise les données pour envoie en db.
   *
   * @param $data
   *
   * @return string
   */
  public static function serializeData($data) {
    return serialize($data);
  }

  /**
   * Déserialise les données issues de la db.
   *
   * @param $data
   *
   * @return mixed
   */
  public static function unserializeData($data) {
    return unserialize($data);
  }

  /**
   * Retourne la configuration du champs.
   *
   * @return array
   */
  public function getConfiguration() {
    $value = $this->getValue();
    if( is_array($value) && array_key_exists(static::FIELD_CONFIGURATION, $value) ){
      return static::unserializeData($value[static::FIELD_CONFIGURATION])?:[];
    }
    return [];
  }

  /**
   * Retourne le plugin
   */
  public function getEasyListBuilderPlugin() {
    return EasyListBuilderPluginManager::me()
      ->getInstanceByPluginID($this->getValue()[static::FIELD_PLUGIN_ID]);
  }

  /**
   * Retourne le easy list builder object
   */
  public function getEasyListBuilder($nbItemsPerPage = 12) {
    if( $this->easy_list_builder === FALSE ){
      $this->easy_list_builder = NULL;
      if ($plugin = $this->getEasyListBuilderPlugin()) {
        $this->easy_list_builder = $plugin->createEasyListBuilder(
          $this->getConfiguration(),
          $nbItemsPerPage,
          $this
        );
      }
    }

    return $this->easy_list_builder;
  }
}
