<?php

namespace Drupal\easy_list_builder\Plugin\Field\FieldFormatter;

use Drupal\easy_list_builder\Cache\Context\EasyListBuilderMainPageCacheContext;
use Drupal\easy_list_builder\Service\EasyListBuilder;
use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\Core\Entity\Entity\EntityViewMode;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'easy_list_builder_field_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "easy_list_builder_field_formatter",
 *   label = @Translation("Easy List Builder field formatter"),
 *   field_types = {
 *     "easy_list_builder_field_type"
 *   }
 * )
 */
class EasyListBuilderFieldFormatter extends FormatterBase {

  /**
   * Settings View mode
   *
   * @const string
   */
  const FIELD_VIEW_MODE = 'view_mode';

  /**
   * FIELD_NUMBER_OF_ITEMS
   *
   * @const string
   */
  const FIELD_NUMBER_OF_ITEMS = 'nb_items';

  /**
   * Builder
   *
   * @var EasyListBuilder
   */
  protected $easyListBuilder;

  /**
   * EasyListBuilderFieldFormatter constructor.
   *
   * @param $plugin_id
   * @param $plugin_definition
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   * @param array $settings
   * @param $label
   * @param $view_mode
   * @param array $third_party_settings
   */
  public function __construct($plugin_id, $plugin_definition, \Drupal\Core\Field\FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);

    $this->easyListBuilder = EasyListBuilder::me();
  }


  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    // Récupération du plugin.
    /** @var \Drupal\easy_list_builder\Interfaces\EasyListBuilderPluginInterface $plugin */
    if ($items->first()) {

      // On ajoute un cache contexte sur l'entité parente.
      if ($entity = $items->getParent()->getEntity()) {
        if ($entity instanceof RefinableCacheableDependencyInterface) {
          $entity->addCacheContexts([EasyListBuilderMainPageCacheContext::CONTEXT_ID]);
        }
      }

      // On instancie le build array.
      /** @var \Drupal\easy_list_builder\Interfaces\EasyListBuilderInterface $easyListBuilder */
      if ($easyListBuilder = $items->first()->getEasyListBuilder($this->getSetting(static::FIELD_NUMBER_OF_ITEMS))) {
        $elements[0]['#easy_list_builder'] = $easyListBuilder;

        // On vérifie le type de contexte.
        // Si on est sur la page principale, on affiche directement le contenu
        // sans passer par le build. Ce contenu sera mis en cache.
        if (EasyListBuilderMainPageCacheContext::me()->isMainPageContext()) {
          $easyListBuilder->setFieldFormatter($this);
          $elements[0]['form'] = $this->easyListBuilder->buildForm($easyListBuilder);
          $elements[0]['list'] = $this->easyListBuilder->buildList($easyListBuilder);
          $elements[0]['pager'] = $this->easyListBuilder->buildPager($easyListBuilder);
          $elements[0]['#easy_list_builder'] = $easyListBuilder;
        }
        else {
          $elements[0]['list'] = $this->getLazyBuildArray($entity, $items->getName(), 'getLazyList');
          $elements[0]['pager'] = $this->getLazyBuildArray($entity, $items->getName(), 'getLazyPager');
          $elements[0]['form'] = $this->getLazyBuildArray($entity, $items->getName(), 'getLazyForm');
        }

      }
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        static::FIELD_VIEW_MODE => 'default',
        static::FIELD_NUMBER_OF_ITEMS => 12,
      ] + parent::defaultSettings();
  }


  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $form[static::FIELD_VIEW_MODE] = [
      '#type'          => 'select',
      '#title'         => $this->t('View mode des éléments de liste'),
      '#default_value' => $this->getSetting(static::FIELD_VIEW_MODE),
      '#options'       => $this->getAllViewModeOptions()
    ];

    $form[static::FIELD_NUMBER_OF_ITEMS] = [
      '#type'          => 'number',
      '#title'         => $this->t('Nombre d\'éléments par page'),
      '#default_value' => $this->getSetting(static::FIELD_NUMBER_OF_ITEMS),
      '#required'      => TRUE,
    ];

    return $form;
  }

  /**
   * Retourne la liste des view modes disponibles.
   *
   * @return mixed
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getAllViewModeOptions() {
    // Récuépration des view modes.
    /** @var \Drupal\Core\Entity\Entity\EntityViewDisplay[] $viewDisplays */
    $viewDisplays = \Drupal::entityTypeManager()
      ->getStorage('entity_view_display')
      ->loadByProperties([]);
    foreach ($viewDisplays as $viewDisplay) {
      $viewModeId = $viewDisplay->getMode();
      if ($viewMode = EntityViewMode::load($viewDisplay->getTargetEntityTypeId() . '.' . $viewModeId)) {
        $options[$viewModeId] = $viewMode->label();
      }
      else {
        $options[$viewModeId] = $viewModeId;
      }
    }

    return $options;
  }

  /**
   * Retourne les éléments pour le lazy builders.
   */
  protected function getLazyBuildArray(EntityInterface $entity, $fieldName, $callbackName) {
    $id = EasyListBuilder::SERVICE_NAME;
    $callbackMethod = $id . ':' . $callbackName;

    // On ajoute le placeholder dans les variables du template.
    return [
      '#attached' => [
        'placeholders' => [
          $callbackName => [
            '#lazy_builder' => [
              $callbackMethod,
              [
                $entity->getEntityTypeId(),
                $entity->id(),
                $fieldName
              ]
            ]
          ]
        ]
      ],
      0           => ['#markup' => $callbackName]
    ];
  }

}
