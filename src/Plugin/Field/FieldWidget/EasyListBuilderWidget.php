<?php

namespace Drupal\easy_list_builder\Plugin\Field\FieldWidget;

use Drupal\easy_list_builder\Interfaces\EasyListBuilderPluginInterface;
use Drupal\easy_list_builder\Plugin\Field\FieldType\EasyListBuilderFieldType;
use Drupal\easy_list_builder\Service\EasyListBuilderPluginManager;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'easy_list_builder_widget' widget.
 *
 * @FieldWidget(
 *   id = "easy_list_builder_widget",
 *   label = @Translation("Easy List Builder widget"),
 *   field_types = {
 *     "easy_list_builder_field_type"
 *   }
 * )
 */
class EasyListBuilderWidget extends WidgetBase {

  /**
   * SETTING de liste à inclure
   *
   * @const string
   */
  const SETTING_INCLUDED_PLUGINS = 'included_plugins';

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    // Gestion du formulaire de settings par défaut.
    $required = strpos(\Drupal::routeMatch()
        ->getRouteName(), 'entity.field_config') !== 0;

    // Details.
    $element['plugin'] = [
      '#type'   => 'details',
      '#title'  => t('Liste'),
      '#open'   => TRUE,
      '#tree'   => TRUE,
      '#access' => $this->getAccess(),
    ];

    // Selecteur de type.
    $availablePlugins = $this->getAvailablePluginsOptionsList();
    $element['plugin'][EasyListBuilderFieldType::FIELD_PLUGIN_ID] = [
      '#type'          => 'select',
      '#title'         => t('Type de liste'),
      '#default_value' => $items->first() ? $items->first()->plugin_id : $this->getSetting(EasyListBuilderFieldType::FIELD_PLUGIN_ID),
      '#required'      => $required,
      '#options'       => !empty($availablePlugins) ? $availablePlugins : $this->getPluginOptionsList(),
    ];

    $this->initPluginsForms($element, $items->first(), $form_state);

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    if ($plugin = EasyListBuilderPluginManager::me()
      ->getInstanceByPluginID($values[0]['plugin'][EasyListBuilderFieldType::FIELD_PLUGIN_ID])) {

      $value = array_key_exists($plugin->pluginId() , $values[0]['plugin']) ? $values[0]['plugin'][$plugin->pluginId()][0] : null;
      $configuration = $plugin->getMassagedWidgetValues($value ?: [], $form, $form_state) ?: [];


      return [
        [
          EasyListBuilderFieldType::FIELD_PLUGIN_ID         => $plugin->pluginId(),
          EasyListBuilderFieldType::FIELD_CONFIGURATION     => EasyListBuilderFieldType::serializeData($configuration),
        ]
      ];
    }
    return [];
  }

  /**
   * Retourne la liste d'option liés aux plugins.
   *
   * @return array
   */
  protected function getPluginOptionsList() {
    return array_map(function (EasyListBuilderPluginInterface $easyListBuilderPlugin) {
      return $easyListBuilderPlugin->label();
    }, EasyListBuilderPluginManager::me()->getAllInstances());
  }

  /**
   * Initialise les formulaires liées au plugins.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  protected function initPluginsForms(array &$element, EasyListBuilderFieldType $item, FormStateInterface $formState) {
    $fieldId = $this->getStateFieldName($element, $item);

    /** @var EasyListBuilderPluginInterface $easyListBuilderPlugin */
    foreach (EasyListBuilderPluginManager::me()
               ->getAllInstances() as $easyListBuilderPlugin) {
      // Définition de la condtion de #states.
      $stateCondition = [':input[name="' . $fieldId . '"]' => ['value' => $easyListBuilderPlugin->pluginId()]];
      // Sous details.
      $element['plugin'][$easyListBuilderPlugin->pluginId()] = [
        '#type'   => 'container',
        '#states' => ['visible' => $stateCondition],
      ];

      // Ajout du sous formulaire.
      $element['plugin'][$easyListBuilderPlugin->pluginId()][] = $easyListBuilderPlugin->getWidgetForm($item, $item->getConfiguration(), $formState, $stateCondition);
    }
  }

  /**
   * Renvoie le nom du champ qui est utilisé pour les states.
   *
   * @param array $element
   * @param $item
   */
  protected function getStateFieldName(array $element, $item) {
    if (count($element["#field_parents"]) > 0) {
      return $element["#field_parents"][0] . '[' . $item->getParent()
          ->getName() . '][0][plugin][plugin_id]';
    }
    else {
      return $item->getParent()
          ->getName() . '[0][plugin][plugin_id]';
    }
  }

  /**
   * Retourne l'état d'accès.
   *
   * @return bool
   */
  protected function getAccess() {
    return \Drupal::currentUser()->hasPermission('access easy list builder widget');
  }

  /**
   * Permet de filtrer les listes disponibles.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    // Récupération de la liste des plugins dispo.
    $pluginList = array_map(function (EasyListBuilderPluginInterface $plugin) {
      return $plugin->label();
    }, EasyListBuilderPluginManager::me()
      ->getAllInstances());

    $form[static::SETTING_INCLUDED_PLUGINS] = [
      '#type'          => 'checkboxes',
      '#title'         => t('Listes disponibles (ne rien cocher pour tout proposer)'),
      '#default_value' => $this->getSetting(static::SETTING_INCLUDED_PLUGINS),
      '#options'       => $pluginList,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    return ['#markup' => implode(', ', $this->getAvailablePluginsOptionsList())];
  }

  /**
   * Retourne les plugins dispo pour ce widget.
   *
   * @return EasyListBuilderPluginInterface[] array
   */
  public function getAvailablePluginsOptionsList() {
    $lists = [];
    foreach (array_filter($this->getSetting(static::SETTING_INCLUDED_PLUGINS)) as $pluginId) {
      $lists[$pluginId] = EasyListBuilderPluginManager::me()
        ->getInstanceByPluginID($pluginId)->label();
    }

    return $lists;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        static::SETTING_INCLUDED_PLUGINS => [],
      ] + parent::defaultSettings();
  }

}
