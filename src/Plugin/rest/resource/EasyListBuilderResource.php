<?php

namespace Drupal\easy_list_builder\Plugin\rest\resource;

use Drupal\Component\Plugin\DependentPluginInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Routing\BcRoute;
use Drupal\easy_list_builder\Parameters\EasyListBuilderParameters;
use Drupal\easy_list_builder\Service\EasyListBuilderManager;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Drupal\easy_list_builder\Interfaces\EasyListBuilderRestInterface;
use \Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Represents Easy List Builder records as resources.
 *
 * @RestResource (
 *   id = "easy_list_builder_resource",
 *   label = @Translation("Easy List Builder"),
 *   uri_paths = {
 *     "canonical" = "/api/list/{listId}",
 *     "https://www.drupal.org/link-relations/create" = "/api/list"
 *   }
 * )
 *
 * @DCG
 * This plugin exposes database records as REST resources. In order to enable it
 * import the resource configuration into active configuration storage. You may
 * find an example of such configuration in the following file:
 * core/modules/rest/config/optional/rest.resource.entity.node.yml.
 * Alternatively you can make use of REST UI module.
 * @see https://www.drupal.org/project/restui
 * For accessing Drupal entities through REST interface use
 * \Drupal\rest\Plugin\rest\resource\EntityResource plugin.
 */
class EasyListBuilderResource extends ResourceBase implements DependentPluginInterface {

  /**
   * @var \Drupal\easy_list_builder\Service\EasyListBuilderManager
   */
  protected EasyListBuilderManager $easyListBuilderManager;

  /**
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected Request $currentRequest;

  /**
   * Constructs a Drupal\rest\Plugin\rest\resource\EntityResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Database\Connection $db_connection
   *   The database connection.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, array $serializer_formats, LoggerInterface $logger, EasyListBuilderManager $easyListBuilderManager, Request $currentRequest) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
    $this->easyListBuilderManager = $easyListBuilderManager;
    $this->currentRequest = $currentRequest;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest'),
      $container->get('easy_list_builder.manager'),
      $container->get('request_stack')->getCurrentRequest()
    );
  }

  /**
   * Responds to GET requests.
   *
   * @param string $listId
   *   The ID of the record.
   *
   * @return \Drupal\rest\ResourceResponse
   *   The response containing the record.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   */
  public function get($listId) {


    $list = $this->easyListBuilderManager->getEasyListBuilder(
      $listId,
      $this->currentRequest->query->get('conf'),
      $this->currentRequest->query->get('nbItemsPerPage')?:12,
    );

    $parameters = EasyListBuilderParameters::create($list);
    

    $result = [
      'info' => $this->easyListBuilderManager->getListInformation($list, $parameters),
    ];
    if( $list instanceof EasyListBuilderRestInterface ){
      $result['list'] = $list->getRestFormatList($parameters);
    }
    else{
      $result['list'] = $list->getList($parameters);
    }
    
    return new JsonResponse($result);
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    return [];
  }

  /**
   * Validates incoming record.
   *
   * @param mixed $record
   *   Data to validate.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\BadRequestHttpException
   */
  protected function validate($record) {

  }

}
