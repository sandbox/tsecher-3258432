<?php

namespace Drupal\easy_list_builder\Service;

use Drupal\easy_list_builder\Interfaces\EasyListBuilderInterface;
use Drupal\easy_list_builder\Parameters\EasyListBuilderParameters;

class EasyListBuilderManager {

  /**
   * Nom du service.
   *
   * @const string
   */
  const SERVICE_NAME = 'easy_list_builder.manager';

  /**
   * Plugin Manager.
   *
   * @var \Drupal\easy_list_builder\Service\EasyListBuilderPluginManager
   */
  protected $pluginmanager;

  /**
   * Builder.
   *
   * @var \Drupal\easy_list_builder\Service\EasyListBuilderBuilder
   */
  protected $builder;

  /**
   * @param \Drupal\easy_list_builder\Service\EasyListBuilderPluginManager $pluginmanager
   */
  public function __construct(EasyListBuilderPluginManager $pluginmanager, EasyListBuilder $builder) {
    $this->pluginmanager = $pluginmanager;
    $this->builder = $builder;
  }

  /**
   * Retourne le singleton.
   *
   * @return static
   *   Le singleton.
   */
  public static function me() {
    return \Drupal::service(static::SERVICE_NAME);
  }

  /**
   * @param $pluginId
   * @param $conf
   * @param $nbItemsPerPage
   *
   * @return \Drupal\easy_list_builder\Interfaces\EasyListBuilderInterface|mixed
   */
  public function getEasyListBuilder($pluginId, $conf = [], $nbItemsPerPage = 12 ) {
    $conf = $conf && is_array($conf)?:[];
    if (is_null($this->easyListBuilders[$pluginId])) {
      if ($plugin = $this->pluginmanager->getInstanceByPluginID($pluginId)) {
        $this->easyListBuilders[$pluginId] = $plugin->createEasyListBuilder(
          $conf,
          $nbItemsPerPage
        );
      }
    }

    return $this->easyListBuilders[$pluginId];
  }

  /**
   * Retourne les informations de la liste
   */
  public function getListInformation(EasyListBuilderInterface $list, EasyListBuilderParameters $parameters){
    return $this->builder->getListInformation($list, $parameters);
  }

}
