<?php

namespace Drupal\easy_list_builder\Service;

use Drupal\easy_list_builder\Annotation\EasyListBuilderPluginAnnotation;
use Drupal\easy_list_builder\Interfaces\EasyListBuilderPluginInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Class EasyListBuilderPluginManager.
 *
 * @package Drupal\easy_list_builder\Service
 */
class EasyListBuilderPluginManager extends DefaultPluginManager {

  /**
   * Nom du service
   *
   * @const string
   */
  const SERVICE_NAME = 'easy_list_builder.plugin_manager';

  /**
   * Plugin Package name
   *
   * @const string
   */
  const PACKAGE_NAME = 'easy_list_builder';

  /**
   * Retourne le singleton.
   *
   * @return static
   *   Le singleton.
   */
  public static function me() {
    return \Drupal::service(static::SERVICE_NAME);
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/' . static::PACKAGE_NAME,
      $namespaces,
      $module_handler,
      EasyListBuilderPluginInterface::class,
      EasyListBuilderPluginAnnotation::class
    );

    $hookName = static::PACKAGE_NAME . '_info';
    $this->alterInfo($hookName);
  }

  /**
   * Retourne toutes les instances de plugins.
   *
   * @return array|\Drupal\Component\Plugin\Mapper\MapperInterface|null
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function getAllInstances() {
    if (!$this->mapper) {
      $this->mapper = [];
      foreach ($this->getDefinitions() as $definition) {
        $this->mapper[$definition['id']] = $this->createInstance($definition['id'], $definition);
      }
    }

    return $this->mapper;
  }

  public function createInstance($plugin_id, array $configuration = []) {
    /** @var EasyListBuilderPluginInterface $plugin */
    if ($plugin = parent::createInstance($plugin_id, $configuration)) {
      $plugin->setPluginDefinitions($configuration);
    }
    return $plugin;
  }

  /**
   * Retourne le plugin par son id.
   * 
   * @param $pluginId
   * 
   * @return EasyListBuilderPluginInterface
   *   Le plugin.
   */
  public function getInstanceByPluginID($pluginId) {
    if (!$this->mapper) {
      $this->getAllInstances();
    }
    if (array_key_exists($pluginId, $this->mapper)) {
      return $this->mapper[$pluginId];
    }
    return NULL;
  }


}
