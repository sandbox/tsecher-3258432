<?php

namespace Drupal\easy_list_builder\Service;

use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\easy_list_builder\Interfaces\EasyListBuilderInterface;
use Drupal\easy_list_builder\Interfaces\EasyListBuilderRenderedDataInterface;
use Drupal\easy_list_builder\Parameters\EasyListBuilderParameters;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class EasyListBuilder implements TrustedCallbackInterface {

  /**
   * Nom du service
   *
   * @const string
   */
  const SERVICE_NAME = 'easy_list_builder.builder';

  /**
   * Attribut list.
   *
   * @const string
   */
  const ATTRIBUTE_LIST = 'data-elb-list';

  /**
   * Attribut form.
   *
   * @const string
   */
  const ATTRIBUTE_FORM = 'data-elb-form';

  /**
   * Attribut form.
   *
   * @const string
   */
  const ATTRIBUTE_PAGER = 'data-elb-pager';

  /**
   * Entity type manager
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Lazy cache
   *
   * @var array
   */
  protected $lazyCache = [];

  /**
   * Retourne le singleton.
   *
   * @return static
   *   Le singleton.
   */
  public static function me() {
    return \Drupal::service(static::SERVICE_NAME);
  }

  /**
   * EasyListBuilder constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   */
  public function __construct(\Drupal\Core\Entity\EntityTypeManager $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Build le formulaire.
   *
   * @param \Drupal\easy_list_builder\Interfaces\EasyListBuilderInterface $easyListBuilder
   *
   * @return mixed
   */
  public function buildForm(EasyListBuilderInterface $easyListBuilder) {

    // Initialisation des paramètres.
    $parameters = EasyListBuilderParameters::create($easyListBuilder);

    if ($form = $easyListBuilder->getForm($parameters)) {
      $this->addEasyListBuilderInfo($form, $easyListBuilder, $parameters);

      return $this->wrapInTemplate($easyListBuilder, $form, static::ATTRIBUTE_FORM, 'easy_list_builder_form', 0);
    }
  }

  /**
   * Build la liste.
   *
   * @param \Drupal\easy_list_builder\Interfaces\EasyListBuilderInterface $easyListBuilder
   *
   * @return mixed
   */
  public function buildList(EasyListBuilderInterface $easyListBuilder) {
    // Initialisation des paramètres.
    $parameters = EasyListBuilderParameters::create($easyListBuilder);

    // On checke si on est pas sur un numéro de page inexistant.
    if ($parameters->getCurrentPage() > 0 && $easyListBuilder->getPageItemsCount($parameters) == 0) {
      // Hors ajax, on 404.
      if (!\Drupal::request()->isXmlHttpRequest()) {
        throw new NotFoundHttpException();
      }
      else {
        $list = [];
      }
    }
    else {
      $list = $easyListBuilder->getList($parameters);
    }

    if (isset($list)) {
      $this->addEasyListBuilderInfo($list, $easyListBuilder, $parameters);
      $listBuildArray = $this->wrapInTemplate($easyListBuilder, $list, static::ATTRIBUTE_LIST, 'easy_list_builder_list', 1);

      return $listBuildArray;
    }
  }

  /**
   * Build le pager.
   *
   * @param \Drupal\easy_list_builder\Interfaces\EasyListBuilderInterface $easyListBuilder
   */
  public function buildPager(EasyListBuilderInterface $easyListBuilder) {
    // Initialisation des paramètres
    $parameters = EasyListBuilderParameters::create($easyListBuilder);
    if ($pager = $easyListBuilder->getPager($parameters)) {
      $this->addEasyListBuilderInfo($pager, $easyListBuilder, $parameters);

      return $this->wrapInTemplate($easyListBuilder, $pager, static::ATTRIBUTE_PAGER, 'easy_list_builder_pager', 2);
    }
  }

  /**
   * Retourne la query de l'url de filtre.
   *
   * @param \Drupal\easy_list_builder\Parameters\EasyListBuilderParameters $parameters
   */
  public function buildUrlQuery(EasyListBuilderInterface $easyListBuilder, EasyListBuilderParameters $parameters) {
    // On récupère les éléments d'url.
    $urlParameters = $easyListBuilder->getUrlQueryFromParameters($parameters);

    // S'ils n'existent pas.
    if (is_null($urlParameters)) {
      $urlParameters = $parameters->getUrlParameters();
    }

    return '?' . http_build_query($urlParameters);
  }

  /**
   * Ajoute les infos au build array.
   *
   * @param $easyListBuilder
   * @param $parameters
   */
  protected function addEasyListBuilderInfo(&$buildArray, EasyListBuilderInterface $easyListBuilder, $parameters) {
    $buildArray['#easy_list_builder_info'] = 
      $this->getListInformation($easyListBuilder, $parameters) 
      + ['easy_list_builder' => $easyListBuilder];

    if ($easyListBuilder instanceof EasyListBuilderRenderedDataInterface) {
      $easyListBuilder->alterBuildArray($buildArray['#easy_list_builder_info'], $parameters);
    }
  }

  /**
   * Initialise les urls des pagers en fonction des
   *
   * @param $variables
   */
  public function initPagerUrls(&$variables, EasyListBuilderParameters $parameters) {
    $urlParameters = $parameters->getUrlParameters();
    if (array_key_exists('page', $urlParameters)) {
      unset($urlParameters['page']);
    }

    if (array_key_exists('items', $variables) && is_array($variables['items'])) {
      array_walk_recursive($variables['items'],
        function (&$value, $key) use ($urlParameters) {
          if ($key === 'href') {
            // On split l'url:
            $newUrl = [];
            parse_str(substr($value, 1), $newUrl);
            // ON supprime les trucs inutiles.
            unset($newUrl['ajax_form']);
            unset($newUrl['_wrapper_format']);
            if ($newUrl['page'] == 0) {
              unset($newUrl['page']);
            }
            // Ajout des filtres si ils sont présents.
            $newUrl = array_merge($newUrl, $urlParameters);

            // On rebuild la requête.
            $value = '?' . http_build_query($newUrl);
          }
        });
    }
  }

  /**
   * Wrap le contenu dans un template dédié.
   *
   * @param \Drupal\easy_list_builder\Interfaces\EasyListBuilderInterface $easyListBuilder
   * @param $content
   * @param $attribute
   * @param $theme
   *
   * @return array
   */
  protected function wrapInTemplate(EasyListBuilderInterface $easyListBuilder, $content, $attribute, $theme, $weight=0) {
    return [
      'content'     => $content,
      '#attributes' => [
        $attribute => $easyListBuilder->getId()
      ],
      '#attached'   => ['library' => ['easy_list_builder/url']],
      '#theme'      => $theme,
      '#weight'      => $weight,
    ];
  }

  /**
   *
   * @param $parentType
   * @param $parentId
   * @param $fieldName
   */
  protected function getEasyListBuilderFromLazyParameters($parentType, $parentId, $fieldName) {
    $id = implode('_', func_get_args());
    if (!array_key_exists($id, $this->lazyCache)) {
      if ($storage = $this->entityTypeManager->getStorage($parentType)) {
        if ($item = $storage->load($parentId)) {
          if ($field = $item->get($fieldName)->first()) {
            $this->lazyCache[$id] = $field->getEasyListBuilder();
          }
        }
      }
    }

    return $this->lazyCache[$id];
  }

  /**
   * Retourne le build array du contenu de la liste.
   */
  public function getLazyList($parentType, $parentId, $fieldName) {
    if ($easyListBuilder = $this->getEasyListBuilderFromLazyParameters($parentType, $parentId, $fieldName)) {
      return $this->buildList($easyListBuilder);
    }

    return [];
  }

  /**
   * Retourne le build array du formulaire.
   */
  public function getLazyForm($parentType, $parentId, $fieldName) {
    if ($easyListBuilder = $this->getEasyListBuilderFromLazyParameters($parentType, $parentId, $fieldName)) {
      return $this->buildForm($easyListBuilder);
    }

    return [];
  }

  /**
   * Retourne le build array du pager.
   */
  public function getLazyPager($parentType, $parentId, $fieldName) {
    if ($easyListBuilder = $this->getEasyListBuilderFromLazyParameters($parentType, $parentId, $fieldName)) {
      return $this->buildPager($easyListBuilder);
    }

    return [];
  }

  /**
   * Retourne les informations courrantes sur l'état de la liste.
   */
  public function getListInformation(EasyListBuilderInterface $easyListBuilder, $parameters){

    $nbPages = floor(($easyListBuilder->getTotalCount($parameters) + $easyListBuilder->getFirstPageDelta($parameters) ) / $easyListBuilder->getNbItemsPerPage());
    $theoricPrevPage = $parameters->getCurrentPage() - 1;
    $theoricNextPage = $parameters->getCurrentPage() + 1;
    
    return [
      'parameters'        => $parameters,
      'data'              => [
        'totalCount'     => $easyListBuilder->getTotalCount($parameters),
        'pageItemsCount' => $easyListBuilder->getPageItemsCount($parameters),
        'nbItemsPerPage' => $easyListBuilder->getNbItemsPerPage(),
        'currentPage'    => $parameters->getCurrentPage(),
        'nbPages'        => $nbPages + 1,
        'previousPage'   => $theoricPrevPage < $nbPages && $theoricPrevPage > -1 ? $theoricPrevPage : null,
        'nextPage'       => $theoricNextPage < $nbPages +1 && $theoricNextPage > 0 ? $theoricNextPage : null,
        'configuration'  => $easyListBuilder->getConfiguration(),
        'other'          => $easyListBuilder->getOtherInfo($parameters)?:[],

      ]
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return [
      'getLazyList',
      'getLazyForm',
      'getLazyPager',
    ];
  }

}
