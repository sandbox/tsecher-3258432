<?php
/**
 * Created by PhpStorm.
 * User: tsecher
 * Date: 09/07/2019
 * Time: 16:03
 */

namespace Drupal\easy_list_builder\Service;


use Drupal\Core\Cache\Cache;
use Drupal\easy_list_builder\Plugin\Field\FieldType\EasyListBuilderFieldType;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManager;

class EasyListBuilderItemHandler {

  /**
   * Nom du service
   *
   * @const string
   */
  const SERVICE_NAME = 'easy_list_builder.item_handler';

  /**
   * ID du champs entité dans le tableau de cache
   *
   * @const string
   */
  const FIELD_ENTITY = 'entity';

  /**
   * ID du champs easy list builder dans le tableau de cache.
   *
   * @const string
   */
  const FIELD_EASY_LIST_BUILDER = 'easy_list_builder';

  /**
   * Cache
   *
   * @var array
   */
  protected $parentListCache = [];

  /**
   * Plugin cache
   *
   * @var array
   */
  protected $pluginCache = [];

  /**
   * Liste de toutes les instances de EasyListBuilder
   *
   * @var \Drupal\easy_list_builder\Interfaces\EasyListBuilderInterface[]
   */
  protected $allEasyListBuilderInstances;

  /**
   * EntityFieldManager
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * EntityTypeManager
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * EasyListBuilderItemHandler constructor.
   *
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   */
  public function __construct(\Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager, EntityTypeManager $entityTypeManager) {
    $this->entityFieldManager = $entityFieldManager;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Retourne le singleton.
   *
   * @return static
   *   Le singleton.
   */
  public static function me() {
    return \Drupal::service(static::SERVICE_NAME);
  }

  /**
   * Retourne l'ensemble des listes auxquelles appartient l'entité passé.
   *
   * @return \Drupal\easy_list_builder\Interfaces\EasyListBuilderInterface[]|
   */
  public function getAllEasyListBuilderWhereEntityIsListed(EntityInterface $entity) {
    $entityId = $this->getEntityAbsoluteId($entity);
    if (!array_key_exists($entityId, $this->parentListCache)) {
      $this->parentListCache[$entityId] = [];
      // On parse tous les fields et on test chaque EasyListBuilderinterface
      foreach ($this->getAllEasyListBuilderInstances() as $easyListBuilderData) {
        /** @var \Drupal\easy_list_builder\Interfaces\EasyListBuilderInterface $easyListBuilder */
        $easyListBuilder = $easyListBuilderData[static::FIELD_EASY_LIST_BUILDER];
        // Si l'entité fait parti de la liste, on ajoute la liste au tableau de données.
        if ($easyListBuilder && $easyListBuilder->entityIsInDefaultList($entity)) {
          if (!isset($this->parentListCache[$entityId])) {
            $this->parentListCache[$entityId] = [];
          }
          $this->parentListCache[$entityId][] = $easyListBuilderData;
        }
      }
    }

    return $this->parentListCache[$entityId];
  }

  /**
   * Retourne l'id de l'entité.
   */
  protected function getEntityAbsoluteId(EntityInterface $entity) {
    return $entity->getEntityTypeId() . '__' . $entity->id();
  }

  /**
   * Retourne toutes les instances de list content
   */
  protected function getAllEasyListBuilderInstances() {
    if (is_null($this->allEasyListBuilderInstances)) {
      $this->allEasyListBuilderInstances = [];

      // ON récupère la liste des fields dans les field types.
      $map = $this->entityFieldManager->getFieldMapByFieldType('easy_list_builder_field_type');

      // Parcours des types d'entité.
      foreach ($map as $entityTypeId => $entityTypeData) {
        /** @var \Drupal\Core\Entity\EntityTypeInterface $entityType */
        if ($entityType = $this->entityTypeManager->getStorage($entityTypeId)) {

          // Récupération de l'id du field bundle
          /** @var \Drupal\Core\Entity\ContentEntityType $definitions */
          $definitions = $this->entityTypeManager->getDefinition($entityTypeId);
          $bundleFieldName = $definitions->getKey('bundle');

          // Parcours des champs
          foreach ($entityTypeData as $fieldName => $fieldData) {
            // Récupération des entités.
            $query = \Drupal::entityQuery($entityTypeId);
            $query->condition($bundleFieldName, $fieldData['bundles'], 'IN');
            $query->exists($fieldName . '.plugin_id');
            $result = $query->execute();
            if (!empty($result)) {
              /** @var EntityInterface $entity */
              foreach ($entityType->loadMultiple($result) as $entity) {
                /** @var EasyListBuilderFieldType $field */
                if ($field = $entity->get($fieldName)->first()) {
                  $this->allEasyListBuilderInstances[$this->getEntityAbsoluteId($entity)] = [
                    static::FIELD_EASY_LIST_BUILDER => $field->getEasyListBuilder(),
                    static::FIELD_ENTITY       => $entity
                  ];
                }
              }
            }
          }
        }
      }
    }
    return $this->allEasyListBuilderInstances;
  }

  /**
   * Retourne la liste des nodes où le easy list builder plugin est utilisé.
   *
   * @param $pluginId
   */
  public function getEntitiesWhereEasyListBuilderPluginIsUsed($pluginId) {
    if (!isset($this->pluginCache[$pluginId])) {
      $this->pluginCache[$pluginId] = [];

      // Récupération de la map.
      $map = $this->entityFieldManager->getFieldMapByFieldType('easy_list_builder_field_type');
      foreach ($map as $entityTypeId => $entityTypeData) {
        /** @var \Drupal\Core\Entity\EntityStorageInterface $entityType */
        if ($entityType = $this->entityTypeManager->getStorage($entityTypeId)) {
          // Parcours des champs
          foreach ($entityTypeData as $fieldName => $fieldData) {
            // Récupération des entités.
            $query = \Drupal::entityQuery($entityTypeId);
            $query->condition($fieldName . '.plugin_id', $pluginId);
            $this->pluginCache[$pluginId] += $query->execute();
          }


          if (!empty($this->pluginCache[$pluginId])) {
            $this->pluginCache[$pluginId] = $entityType->loadMultiple($this->pluginCache[$pluginId]);
          }
        }
      }
    }

    return $this->pluginCache[$pluginId];
  }


  /**
   * Vide les caches de la liste où est listé l'entité.
   */
  public function cleanListCache($entity) {
    $contentList = $this->getAllEasyListBuilderWhereEntityIsListed($entity);
    $entitiesToClean = [];
    foreach ($contentList as $item){
      $entitiesToClean[] = $contentList[0]['entity']->getEntityTypeId().':'.$item['entity']->id();
    }

    if( !empty($entitiesToClean) ){
      Cache::invalidateTags($entitiesToClean);
    }
  }

}
