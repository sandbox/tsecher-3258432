<?php
/**
 * Created by PhpStorm.
 * User: tsecher
 * Date: 22/02/2019
 * Time: 16:35
 */

namespace Drupal\easy_list_builder;


use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

/**
 * Class EasyListBuilderServiceProvider.
 *
 * Redéfinition du service form_builder.
 * Redéfinition du service render_cache.
 *
 * @package Drupal\config_alert
 */
class EasyListBuilderServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    parent::alter($container);

    // Surcharge du form_builder pour ne pas redéfinir une deuxième fois l'entité objet.
    $definition = $container->getDefinition('form_builder');
    $definition->setClass('Drupal\easy_list_builder\Decorador\FormBuilderDecorador');

    // Surcharge du render_cache
    $definition = $container->getDefinition('render_cache');
    $definition->setClass('Drupal\easy_list_builder\Decorador\PlaceholderingRenderCacheDecorador');


  }

}
