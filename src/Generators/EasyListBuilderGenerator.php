<?php


namespace Drupal\easy_list_builder\Generators;


use Drupal\search_api\Entity\Index;
use DrupalCodeGenerator\Command\BaseGenerator;
use DrupalCodeGenerator\Utils;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class EasyListBuilderGenerator extends BaseGenerator {

  protected $name = 'easy-list-builder';
  protected $description = 'Generates a woot.';
  protected $alias = 'g:cl';
  protected $templatePath = __DIR__;

  /**
   * Module handler
   *
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  protected $moduleHandler;

  /**
   * EasyListBuilderGenerator constructor.
   *
   * @param null $moduleHandler
   * @param null $name
   */
  public function __construct($moduleHandler = NULL, $name = NULL) {
    parent::__construct($name);
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  protected function interact(InputInterface $input, OutputInterface $output) {
    $questions = [];
    $questions['machine_name'] = new Question('Module machine name');
    $questions['machine_name']->setValidator([
      Utils::class,
      'validateMachineName'
    ]);
    $questions['label'] = new Question('Label de la liste');
    $questions['label']->setValidator([Utils::class, 'validateRequired']);
    $default_class = function ($vars) {
      return Utils::camel2machine($vars['label']);
    };
    $questions['pluginId'] = new Question('Id du plugin', $default_class);
    $questions['description'] = new Question('Description de la liste');
    $questions['filters'] = new ChoiceQuestion('L\'utilisateur aura-t-il accès à des filtres ?', [
      1 => 'Oui',
      0 => 'Non'
    ], 1);
    $questions['searchApi'] = new ChoiceQuestion('La requête utilisera-t-elle la search_api ?', [
      1 => 'Oui',
      0 => 'Non'
    ], 1);
    $vars = $this->collectVars($input, $output, $questions);
    if ($this->vars['searchApi'] == 'Oui'){
      $questions['search_api_index'] = new ChoiceQuestion('Quel sera l\'index utilisé ?',
        array_map(function(Index $index){return $index->label();}, Index::loadMultiple())
      );
      $questions['search_api_index']->setValidator([Utils::class, 'validateRequired']);
    }

    $questions['entities'] = new ChoiceQuestion('La liste va-t-elle remonter un seul type d\'entité ?', [
      1 => 'Oui',
      0 => 'Non'
    ], 1);
    $vars = $this->collectVars($input, $output, $questions);
    if ($this->vars['entities'] == 'Oui'){
      $questions['entity_type'] = new Question('Quelle sera le type d\'entité utiliser ?', 'node');
      $questions['entity_type']->setValidator([Utils::class, 'validateRequired']);
    }

    $questions['page_delta'] = new ChoiceQuestion('Y aura-t-il un nombre d\'éléments à remonter différent selon la page ?', [
      1 => 'Oui',
      0 => 'Non'
    ], 'Non');

    $questions['facade'] = new ChoiceQuestion('Créer une façade ?', [
      1 => 'Oui',
      0 => 'Non'
    ], 'Oui');
    $vars = $this->collectVars($input, $output, $questions);

    // Génération des templates.
    $this->generateTemplates();
  }

  /**
   * Génération des templates.
   */
  protected function generateTemplates() {
    $camelCase = Utils::camelize($this->vars['pluginId']);
    $this->vars['listClassName'] = $camelCase.'EasyListBuilder';
    $this->vars['pluginClassName'] = $camelCase.'ListPlugin';

    // Création du plugin.
    $this->addFile()
      ->path('src/Plugin/easy_list_builder/' . $this->vars['pluginClassName'] . '.php')
      ->template('templates/easy-list-builder-plugin.twig');

    if( $this->vars['facade'] == 'Non' ){
      $this->addFile()
        ->path('src/EasyListBuilder/' . $this->vars['listClassName'] . '.php')
        ->template('templates/easy-list-builder.twig');
    }
    else{
      $className = $this->vars['listClassName'];
      $this->vars['listClassNameFacade']  = $this->vars['listClassName'].'Facade';
      $this->addFile()
        ->path('src/EasyListBuilder/Facade/' . $this->vars['listClassNameFacade'] . '.php')
        ->template('templates/easy-list-builder.twig');

      $this->vars['listClassName'] = $className;
      $this->addFile()
        ->path('src/EasyListBuilder/' . $this->vars['listClassName'] . '.php')
        ->template('templates/easy-list-builder-facade.twig');
    }
  }


}
