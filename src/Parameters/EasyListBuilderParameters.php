<?php

namespace Drupal\easy_list_builder\Parameters;


use Drupal\easy_list_builder\Interfaces\EasyListBuilderInterface;
use Drupal\Core\Form\FormStateInterface;

class EasyListBuilderParameters {

  /**
   * Page key
   *
   * @const string
   */
  const KEY_PAGE = 'page';

  /**
   * Cache des paramètres.
   *
   * @var \Drupal\easy_list_builder\Parameters\EasyListBuilderParameters[]
   */
  protected static $cache = [];

  /**
   * Formstate
   *
   * @var \Drupal\Core\Form\FormStateInterface
   */
  protected $formState;

  /**
   * Easy List Builder
   *
   * @var EasyListBuilderInterface
   */
  protected $easyListBuilder;

  /**
   * Liste des paramètres
   *
   * @var array
   */
  protected $parameters;

  /**
   * Parameters manipulator
   *
   * @var \Drupal\easy_list_builder\Interfaces\EasyListBuilderParametersManipulatorInterface
   */
  protected $manipulator;

  /**
   * Cache Url
   *
   * @var array
   */
  protected $urlParameters;

  /**
   * Création de l'élément.
   *
   * @param \Drupal\easy_list_builder\Interfaces\EasyListBuilderInterface $easyListBuilder
   * @param \Drupal\Core\Form\FormStateInterface $formState
   * @param string $cacheSuffix
   *
   * @return \Drupal\easy_list_builder\Parameters\EasyListBuilderParameters
   */
  public static function create(EasyListBuilderInterface $easyListBuilder, FormStateInterface $formState = NULL, $cacheSuffix = 'default') {
    $cacheId = $easyListBuilder->getPlugin()->pluginId() . $cacheSuffix;
    if (!array_key_exists($cacheId, static::$cache)) {
      static::$cache[$cacheId] = new static($easyListBuilder, $formState);
    }

    if (!static::$cache[$cacheId]->getFormState()) {
      static::$cache[$cacheId]->setFormState($formState);
    }

    return static::$cache[$cacheId];
  }

  /**
   * EasyListBuilderParameters constructor.
   *
   * @param \Drupal\Core\Form\FormStateInterface $formState
   */
  protected function __construct(EasyListBuilderInterface $easyListBuilder, FormStateInterface $formState = NULL) {
    $this->formState = $formState;
    $this->EasyListBuilder = $easyListBuilder;
    $this->manipulator = $easyListBuilder->getParametersManipulator();

    // Initialise les paramètres.
    $this->initParameters();
  }

  /**
   * @return \Drupal\Core\Form\FormStateInterface
   */
  public function getFormState() {
    return $this->formState;
  }

  /**
   * @param \Drupal\Core\Form\FormStateInterface $formState
   */
  public function setFormState($formState) {
    $this->formState = $formState;

    // Initialise les paramètres.
    $this->initParameters();
  }

  /**
   * Initialise les paramtres.
   */
  protected function initParameters() {
    $this->parameters = [];

    // Paramètres url.
    $this->mergeParams($this->manipulator->getParametersFromUrlQuery(\Drupal::request()->request->all()));

    // Paramètres get.
    $query = \Drupal::request()->query;
    $queryParams = $this->manipulator->getParametersFromUrlQuery($query->all());
    $this->mergeParams($queryParams);

    // Paramètres post.
    if ($this->getFormState()) {
      $this->mergeParams($this->getFormState()->getValues());
    }

    // Point d'entrée sur les paramètres.
    $this->manipulator->initParameters($this->parameters);

    // Gestion du pager.
    if (!$this->getFormState()) {
      if ($query->has(static::KEY_PAGE)) {
        $page = $query->get(static::KEY_PAGE);
        if ($page != 0) {
          $this->parameters[static::KEY_PAGE] = $page;
        }
      }
    }
    else {
      if (array_keys(static::KEY_PAGE, $this->parameters)) {
        unset($this->parameters[static::KEY_PAGE]);
      }
    }

    ksort($this->parameters);

  }

  /**
   * Retourne la liste sans les paramètres.
   *
   * @return $this
   */
  public function clearParameters() {
    $this->parameters = [];
    return $this;
  }

  /**
   * Merge les paramètres.
   *
   * @param $data
   */
  protected function mergeParams($data) {
    if (is_array($data) && !empty($data)) {
      $this->parameters = array_merge($this->parameters, $data);
    }
  }

  /**
   * Retourne la valeur d'un paramètre.
   */
  public function get($key = NULL) {
    if ($key) {
      if (array_key_exists($key, $this->parameters)) {
        return $this->parameters[$key];
      }
      return NULL;
    }

    return $this->parameters;
  }

  /**
   * Retourne l'index de la page courante.
   *
   * @return int
   */
  public function getCurrentPage() {
    return $this->get(static::KEY_PAGE) ?: 0;
  }

  /**
   * Retourne les urls de la query.
   *
   */
  public function getUrlParameters() {
    if (is_null($this->urlParameters)) {
      $parameters = $this->parameters;

      // on supprile le numéro de première page.
      if (array_key_exists(static::KEY_PAGE, $parameters) && $parameters[static::KEY_PAGE] == 0) {
        unset($parameters[static::KEY_PAGE]);
      }

      $this->urlParameters = $this->manipulator->getUrlQueryFromParameters($parameters);
      ksort($this->urlParameters);

    }

    return $this->urlParameters;
  }

}
