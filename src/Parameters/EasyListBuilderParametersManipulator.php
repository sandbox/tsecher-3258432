<?php

namespace Drupal\easy_list_builder\Parameters;


use Drupal\easy_list_builder\Interfaces\EasyListBuilderParametersManipulatorInterface;

class EasyListBuilderParametersManipulator implements EasyListBuilderParametersManipulatorInterface {

  /**
   * TYPE Checkboxes
   *
   * @const string
   */
  const TYPE_CHECKBOXES = 'checkboxes';

  /**
   * TYPE string
   *
   * @const string
   */
  const TYPE_STRING = 'string';

  /**
   * Type default
   */
  const TYPE_DEFAULT = 'default';

  /**
   * Separateur mulitple
   *
   * @const string
   */
  const MULTIPLE_SEPARATOR = '__';

  /**
   * liste de champs.
   *
   * @var array
   */
  protected $parameters;

  /**
   * {@inheritdoc}
   */
  public function add($name, $type, $multiple = FALSE) {
    $this->addCustom(
      $name,
      [$this, 'toUrl_' . $type],
      [$this, 'toParameters_' . $type],
      [$this, 'init_' . $type],
      $multiple
    );
  }

  /**
   * {@inheritdoc}
   */
  public function addCustom($name, $toUrlCallback = NULL, $toParametersCallback = NULL, $initCallback = NULL, $multiple = FALSE) {
    $this->parameters[$name] = [
      'toUrl'        => $this->checkMethod($toUrlCallback) ? $toUrlCallback : NULL,
      'toParameters' => $this->checkMethod($toParametersCallback) ? $toParametersCallback : NULL,
      'init'         => $this->checkMethod($initCallback) ? $initCallback : NULL,
      'multiple'     => $multiple
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getUrlQueryFromParameters(array $parameters) {

    return $this->getParsedFields(
      'toUrl',
      $parameters,
      [$this, 'implode']
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getParametersFromUrlQuery(array $query) {

    return $this->getParsedFields(
      'toParameters',
      $query,
      [$this, 'explode']
    );
  }

  /**
   * {@inheritdoc}
   */
  public function initParameters(array &$parameters) {
    $parameters = $this->getParsedFields(
      'init',
      $parameters,
      [$this, 'explode']
    );
  }

  /**
   * Parcours de la liste en entrée inlist et joue une action.
   *
   * @param $action
   * @param $inList
   * @param $multipleAction
   *
   * @return array
   */
  protected function getParsedFields($action, $inList, $multipleAction) {
    $finalParameters = [];

    if (is_array($this->parameters)) {
      foreach ($this->parameters as $name => $data) {
        // On récupère la valeur actuelle.
        if (array_key_exists($name, $inList)) {
          // On assigne d'abord la valeur par défaut de l'url.
          $finalParameters[$name] = $inList[$name];

          // Ensuite on check les altérants.
          if ($data[$action]) {
            $result = call_user_func_array($data[$action],
              [
                &$finalParameters[$name],
                $inList,
                $data,
                $this->parameters
              ]);
            if (isset($result)) {
              $finalParameters[$name] = $result;
            }
          }

          // Action sur le multiple
          call_user_func_array($multipleAction, [
            $data['multiple'],
            &$finalParameters[$name],
            $name,
            $data,
            $this->parameters
          ]);
        }
      }
    }

    return array_filter($finalParameters);
  }

  /**
   * Action sur l'implode général.
   *
   * @param $isMultiple
   * @param $value
   * @param $name
   * @param $parameterData
   * @param $allParameters
   */
  protected function explode($isMultiple, &$value, $name, $parameterData, $allParameters) {
    // On checke le multiple.
    if ($isMultiple) {
      if (is_string($value)) {
        $value = explode(static::MULTIPLE_SEPARATOR, $value);
      }
    }
  }

  /**
   * Implode les données si array et multiple.
   *
   * @param $isMultiple
   * @param $value
   * @param $name
   * @param $parameterData
   * @param $allParameters
   */
  protected function implode($isMultiple, &$value, $name, $parameterData, $allParameters) {
    if ($isMultiple) {
      if (is_array($value)) {
        $value = implode(static::MULTIPLE_SEPARATOR, $value);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function hasParams(EasyListBuilderParameters $parameters) {
    // TODO: Implement hasParams() method.
  }

  /**
   * {@inheritdoc}
   */
  public function getAllowedParameters() {
    return array_keys($this->parameters);
  }

  /**
   * Teste la validité de la méthode.
   *
   * @param $data
   */
  protected function checkMethod($data) {
    if (is_array($data)) {
      return method_exists(reset($data), end($data));
    }
    elseif (is_string($data)) {
      return function_exists($data);
    }
    return FALSE;
  }

  /**======================================================
   * ||                                                   ||
   * ||                  altérants                      ||
   * ||                                                   ||
   * =======================================================*/
  /**
   * Checkboxes to parameters
   */
  protected function toParameters_checkboxes(&$value, $query, $parameterData, $allParameters) {
    if (is_string($value)) {
      $value = explode(static::MULTIPLE_SEPARATOR, $value);
      $value = $this->filterCheckboxes($value);
    }
  }

  /**
   * Checkboxes to url.
   */
  protected function toUrl_checkboxes(&$value, $query, $parameterData, $allParameters) {
    if (!empty($value)) {
      $final = $this->filterCheckboxes($value);
      if (!empty($final)) {
        $value = implode(static::MULTIPLE_SEPARATOR, $final);
      }
      else {
        $value = NULL;
      }
    }
    else {
      $value = NULL;
    }
  }

  /**
   * Init checkboxes.
   */
  protected function init_checkboxes(&$value, $query, $parameterData, $allParameters) {
    if (is_array($value)) {
      $value = $this->filterCheckboxes($value);
    }
  }

  protected function filterCheckboxes(array $value) {
    return array_filter($value, function ($data) {
      return $data !== 0;
    });
  }
}
