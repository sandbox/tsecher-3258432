<?php

namespace Drupal\easy_list_builder\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Define a Plugin annotation object.
 *
 * @Annotation
 *
 * @ingroup
 */
class EasyListBuilderPluginAnnotation extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * Label : The label.
   *
   * @var string
   */
  public $label;

  /**
   * Description.
   *
   * @var string
   */
  public $description;

}
