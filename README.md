# Easy List Builder

## Introduction
Ce module permet de gérer des liste de contenu plus simplement. Il s'occupe de toute la gestion
hors formulaire et rendu d'éléments.


## Composer

## Principe
Le module met à disposition un type de plugin permettant de créer des outils facilitant la gestion des listes
pour ne pas avoir à systématiquement recréer le processus de gestion de liste (ajax / pager etc...)

### Field type et formatter
Pour cela le module passe par un type de champ EasyListBuilderFieldType. Associé au formatter EasyListBuilderFieldFormatter,
il permet d'afficher les 3 éléments utilses d'une liste  : 
- formulaire de filtre (facultatif)
- contenu (liste des éléments à remonter)
- pager
Pour cela, le champ se base sur deux éléments : 
- le plugin EasyListBuilderPlugin qui gère les liens avec le champs (donc également la partie configuration)
- l'objet qui implémente EasyListBuilderInterface, qui gère la liste en soit.

### Le plugin 
@todo

### La EasyListBuilderInterface
@todo

#### Depuis un formulaire
@todo

#### Gestion du noeud
@todo
